﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class profile_pic_gallery_script : MonoBehaviour
{
    public static profile_pic_gallery_script Instance;

    public Image Profile_pic;
    public string filename;
    public string extension;



    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Ongallery_click()
    {
        PickImage(512);
    }


    private void PickImage(int maxSize)
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {

            filename = Path.GetFileName(path);

            string a = Path.GetExtension(path);
            a = a.Replace(".", string.Empty);
            extension = a;
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
             
                Profile_pic.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                Profile_pic.sprite.name = filename;
                //Save_image_to_file(texture);
                Guest_login_script.Instance.Save_image_to_file(texture);
            }
        }, "Select a PNG image", "image/png");

        //Debug.Log("Permission result: " + permission);
    }

    //public void Save_image_to_file(Texture2D tex)
    //{
    //    byte[] itemBGBytes = tex.EncodeToPNG();
    //    File.WriteAllBytes(Application.persistentDataPath + "/profile_pic/temp.png", itemBGBytes);
    //}


    //public void FileChk()
    //{
    //    if (!Directory.Exists(Application.persistentDataPath+ "/profile_pic"))
    //    {
    //        //if it doesn't, create it
    //        Directory.CreateDirectory(Application.persistentDataPath + "/profile_pic");

    //    }
    //    string filePath = Application.persistentDataPath + "/profile_pic/temp.png";

    //    if (System.IO.File.Exists(filePath))
    //    {
    //        // The file exists -> run event
    //        File.Delete(filePath);
    //    }
    //    else
    //    {
    //        // The file does not exist -> run event
    //    }
    //}

}


