﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Set_User_data : MonoBehaviour
{
    public static Set_User_data Instance;

    public Image Profile_pic;
    public Image profile_pic_home_panel;
    public TextMeshProUGUI username;
    public string profile_pic_link_2;
    public string Username_server;
    string profile_pic_link_1 = "http://134.209.103.120/Casino/public";
    public long credits;
    public int currentxp;
    //public string Token;

    public string code;
    public string Name;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Username_server= "guest_1241444";
        //Set_username();
        //profile_pic_link_2 = "/uploads/profile/priyag_78375.png"; 
        //Set_profilepic();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void Get_all_data_from_playerprefs()
    {
        Get_API_Data_IAP.Instance.Get_settings_data();
    }


    public void setdata_after_get()
    {
        Username_server = PlayerPrefs.GetString("Username");
        Set_username();
        profile_pic_link_2 = PlayerPrefs.GetString("profile_pic_link_2");
        currentxp = PlayerPrefs.GetInt("CurrentXP");
        credits = long.Parse(PlayerPrefs.GetString("Slot_credits"));
        //Get_saved_profile_pic();
        Set_profilepic();

    }

    public void Set_data()
    {
        PlayerPrefs.SetString("Username", Username_server);
        PlayerPrefs.SetString("profile_pic_link_2", profile_pic_link_2);
        Set_username();
        Set_profilepic();
        Set_xp_coin();
        Set_id_mail_logintype();
        if (Splash_Panel_scripts.Instance.gameObject.activeSelf)
        {
            Get_API_Data_IAP.Instance.Get_settings_data();
        }
    }

    public void Set_id_mail_logintype()
    {

        if (Send_Data_to_SErver.Instance.login_type != string.Empty)
        {
            PlayerPrefs.SetString("login_type", Send_Data_to_SErver.Instance.login_type);
            if (Send_Data_to_SErver.Instance.login_type == "google")
            {
                PlayerPrefs.SetString("id", Send_Data_to_SErver.Instance.fb_id);
                PlayerPrefs.SetString("email", Send_Data_to_SErver.Instance.email);
            }
        }

    }
    public void Set_xp_coin()
    {
        PlayerPrefs.SetInt("CurrentXP", currentxp);
        PlayerPrefs.SetString("Slot_credits", credits.ToString());
        //Get_API_Data_IAP.Instance.Get_settings_data();
    }
    public void Set_username()
    {
        string rev = Reverse(Username_server);
        string[] splitArray = rev.Split(char.Parse("_"));
        code = Reverse(splitArray[0]);
        code = "_" + code;
        Name = string.Empty;
        for (int i = 1; i < splitArray.Length; i++)
        {
            if (i == 1)
            {
                Name += splitArray[i];
            }
            else
            {
                Name += "_" + splitArray[i];
            }
        }
        Name = Reverse(Name);
        username.text = Name + "<size= 25><#828282>" + code + "</color></size>";
        //PlayerPrefs.SetString("", Send_Data_to_SErver.Instance.email);
    }
    public void Set_profilepic()
    {
        profile_pic_link_2 = PlayerPrefs.GetString("profile_pic_link_2");
        StartCoroutine("downloadImg", profile_pic_link_1 + profile_pic_link_2);
        //Debug.Log(profile_pic_link_1 + profile_pic_link_2);
    }


    public void set_google_profile_link(string url)
    {
        StartCoroutine("downloadImg", url);
    }

    IEnumerator downloadImg(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        profile_pic_home_panel.sprite = Profile_pic.sprite = image;
        Guest_login_script.Instance.Save_image_to_file(texture);
    }
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }



    public void Get_saved_profile_pic()
    {
        if (System.IO.File.Exists(Application.persistentDataPath + "/profile_pic/temp.png"))
        {
            profile_pic_home_panel.sprite = Profile_pic.sprite = LoadSprite(Application.persistentDataPath + "/profile_pic/temp.png");
        }
        else
        {
            Set_profilepic();
        }
    }

    private Sprite LoadSprite(string path)
    {
        if (string.IsNullOrEmpty(path)) return null;
        if (System.IO.File.Exists(path))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            return sprite;
        }
        return null;
    }
}
