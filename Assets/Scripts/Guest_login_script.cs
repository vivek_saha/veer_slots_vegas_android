﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public class Guest_login_script : MonoBehaviour
{

    public static Guest_login_script Instance;
    //public TMP_InputField Name;
    public Texture2D temp_tex;
    public byte[] itemBGBytes;



    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void Check_Previous_login()
    {

    }
    public void Onlogin_guest()
    {
        //if(PlayerPrefs.SetInt("GuestLogin"))
        //PlayerPrefs.SetInt("logged in", 1);

        Send_Data_to_SErver.Instance.login_type = "guest";
        Send_Data_to_SErver.Instance.Name = "Guest";
        Send_Data_to_SErver.Instance.wallet_coin = 0;
        Send_Data_to_SErver.Instance.current_xp = 0;
        PlayerPrefs.SetInt("GuestLogin", 1);
        Save_image_to_file(temp_tex);
        Send_Data_to_SErver.Instance.Upload_data();
        Splash_Panel_scripts.Instance.Loading_screen();
        //PlayerPrefs.SetInt("logged in", 1);
    }



    //IEnumerator Loading_screen()
    //{
    //    yield return new WaitForSeconds(2f);

    //}


    //For_all_profile_pics
    public void Save_image_to_file(Texture2D tex)
    {
        tex = duplicateTexture(tex);
        itemBGBytes = tex.EncodeToPNG();
        FileChk();
        File.WriteAllBytes(Application.persistentDataPath + "/profile_pic/temp.png", itemBGBytes);
    }


    public void FileChk()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/profile_pic"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/profile_pic");

        }
        string filePath = Application.persistentDataPath + "/profile_pic/temp.png";

        if (System.IO.File.Exists(filePath))
        {
            // The file exists -> run event
            File.Delete(filePath);
        }
        else
        {
            // The file does not exist -> run event
        }
    }

    Texture2D duplicateTexture(Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
                    source.width,
                    source.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);

        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }
}
