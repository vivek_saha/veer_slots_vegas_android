﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class particle_handleer_5OK : MonoBehaviour
{
    public float waittime;
    public GameObject[] particles;
    public GameObject[] images;
    public SpriteRenderer sp_final;
    bool shine = false;
    float x = 0;
    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        shine = false;
    }


    private void OnEnable()
    {
        x = 0;
        shine = false;
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(waittime);
        playparticle();
    }
    // Update is called once per frame
    void Update()
    {
        if(shine)
        {
            x = Mathf.Lerp(x, 1,Time.deltaTime);
            //Debug.Log(x);
            sp_final.material.SetFloat("_ShineLocation", x);
        }
    }

    public void playparticle()
    {
        Invoke("disable_images", 1f);
        //disable_images();
        foreach (GameObject a in particles)
        {
            a.SetActive(true);
        }
    }
    public void stopparticle()
    {
        foreach (GameObject a in particles)
        {
            a.SetActive(false);
        }
    }

    private void OnDisable()
    {
        stopparticle();
    }

    public void disable_images()
    {
        for(int i=0;i<images.Length;i++)
        {
            images[i].SetActive(false);
        }
        sp_final.gameObject.SetActive(true);
        //sp_final.material.SetFloat("ShineLocation",);
        shine = true;
    }


}
