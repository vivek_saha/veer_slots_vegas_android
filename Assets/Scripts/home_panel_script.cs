﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class home_panel_script : MonoBehaviour
{
    public RawImage img;
    public GameObject home_particle;
    public float speed;
    GameObject ab;
    // Start is called before the first frame update
    void Start()
    {
        //img = GetComponent<RawImage>();
    }


    private void OnEnable()
    {
        ab = Instantiate(home_particle, Vector3.zero, Quaternion.identity);

          ab.GetComponent<ParticleSystem>().Play();
    }

    private void OnDisable()
    {
        //home_particle.GetComponent<ParticleSystem>().Stop();
        Destroy(ab);
    }
    

    // Update is called once per frame
    void Update()
    {
        Rect rect = img.uvRect;
        rect.x += speed;
        img.uvRect = rect;
    }
}
