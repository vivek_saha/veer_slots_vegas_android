﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Paytable_Script : MonoBehaviour
{
    public GameObject[] containers;
    public string[] Name;
    Slot slot;

    public void start_paytable_script()
    {

        GameObject a = GameObject.FindGameObjectWithTag("Slot");
        slot = a.GetComponent<Slot>();

        Change_paytable();

    }
    public void Change_paytable()
    {
        //print(containers.Length);
        for (int i = 0; i < containers.Length; i++)
        {
            //Debug.Log("slot.symbolPrefabs[0].GetComponent<SpriteRenderer>().sprite.name" + slot.symbolPrefabs[0].GetComponent<SpriteRenderer>().sprite.name);
            //for icon
            if (Lvl_manager.Instance.lvl_theme <= Lvl_manager.Instance.lvl_cap_server)
            {
                containers[i].gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Level_Design/" + Lvl_manager.Instance.lvl_theme.ToString() + "/icons/" + Name[i]);
            }
            else
            {
                containers[i].gameObject.transform.GetChild(0).GetComponent<Image>().sprite = slot.symbolPrefabs[i].GetComponent<SpriteRenderer>().sprite;
            }
            //for Text
            if (i < 10)
                containers[i].gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = GetPays_text(i);
            if (i == 11)
            {
                containers[i].gameObject.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Level_Design/Wild_icons/" + Lvl_manager.Instance.lvl_theme.ToString());

            }
        }
    }
    public string GetPays_text(int a)
    {
        string st = null;
        //print("slot=" + slot.setPays[]);
        st += "5x - " + (int)(slot.setPays[a].pays[4] * 100) + "\n";
        st += "4x - " + (int)(slot.setPays[a].pays[3] * 100) + "\n";
        st += "3x - " + (int)(slot.setPays[a].pays[2] * 100);

        return st;
    }
}
