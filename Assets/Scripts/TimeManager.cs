﻿///// <summary>
///// Created by Leaton Mitchell 11/17/2017
///// Leatonm.net
///// </summary>
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Networking;
//using UnityEngine.UI;

//public class TimeManager : MonoBehaviour
//{

//	public static TimeManager sharedInstance = null;
//	private string _url = "http://134.209.103.120/VideoCall/public/getDateTime.php";
//	//change this to your own
//	private string _timeData;
//	private string _currentTime;
//	private string _currentDate;


//	//make sure there is only one instance of this always.
//	void Awake()
//	{
//		if (sharedInstance == null)
//		{
//			sharedInstance = this;
//		}
//		else if (sharedInstance != this)
//		{
//			Destroy(gameObject);  
//		}
//		DontDestroyOnLoad(gameObject);
//	}


//	//time fether coroutine
//	public IEnumerator getTime()
//	{
//		//Debug.Log("connecting to php");
//		WWW www = new WWW(_url);
//		yield return www;
//		if (www.error != null)
//		{
//			Debug.Log("Error"+www.error);
//			//Get_API_Data_IAP.Instance.req_call = true;

//            Check_Internet_connection();

//		}
//		else
//		{
//			//Debug.Log("got the php information");
//		_timeData = www.text;
//		string[] words = _timeData.Split('/');	
//		//timerTestLabel.text = www.text;
//		//Debug.Log("The date is : " + words[0]);
//		//Debug.Log("The time is : " + words[1]);

//		//setting current time
//		_currentDate = words[0];
//		_currentTime = words[1];
//		}
//	}


//	public void Check_Internet_connection()
//	{
//		StartCoroutine(Ck_net(isConnected =>
//		{
//			if (isConnected)
//			{
//				Debug.Log("Internet Available!");
//				//return true;
//				if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
//				{
//					Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
//					Splash_Panel_scripts.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
//					Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(true);
//				}
//			}
//			else
//			{
//				Debug.Log("Internet Not Available");
//				//return  false;
//				if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
//				{
//					Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
//					Splash_Panel_scripts.Instance.no_internet_connection_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
//					Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(true);
//				}
//			}
//		}));

//	}

//	public IEnumerator Ck_net(Action<bool> syncResult)
//	{
//		const string echoServer = "http://google.com";

//		bool result;
//		using (var request = UnityWebRequest.Head(echoServer))
//		{
//			request.timeout = 0;
//			yield return request.SendWebRequest();
//			result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
//		}
//		syncResult(result);
//	}
//	//public bool check_net()
//	//{
//	//	WWW www = new WWW("http://www.google.com");
//	//	//yield return www;
//	//	if (www.error != null)
//	//	{
//	//		return false;
//	//	}
//	//	else
//	//	{
//	//		return true;
//	//	}

//	//}
//	public void Retry_connection()
//	{
//		StartCoroutine("getTime");
//		Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
//		Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
//		Splash_Panel_scripts.Instance.Loading_panel.SetActive(true);
//		StartCoroutine("Retry_connection_coroutine");
//	}
//	IEnumerator Retry_connection_coroutine()
//	{
//		yield return new WaitForSeconds(3f);
//		//StartCoroutine("check_connection");
//		Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
//		Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
//		StartCoroutine("getTime");
//	}
//	public void close_Retry_panel()
//	{
//		Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
//		Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
//		Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
//		Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
//	}

//	//get the current time at startup
//	void Start()
//	{
//		//Debug.Log("TimeManager script is Ready.");
//		StartCoroutine("getTime");
//	}

//	//get the current date - also converting from string to int.
//	//where 12-4-2017 is 1242017
//	public int getCurrentDateNow()
//	{
//		string[] words = _currentDate.Split('-');
//		int x = int.Parse(words[0] + words[1] + words[2]);
//		//if()
//		//if(PlayerPrefs.GetInt("First_day",0)==0)
//  //      {
//  //          PlayerPrefs.SetInt("First_day", 1);
//  //      }
//		return x;
//	}


//	//get the current Time
//	public string getCurrentTimeNow()
//	{
//		return _currentTime;
//	}


//}
