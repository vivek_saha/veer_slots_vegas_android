﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderBoard_container_script : MonoBehaviour
{

    public TextMeshProUGUI Number;
    public TextMeshProUGUI lvl_text;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI code;
    public TextMeshProUGUI coin;

    public Image profile_pic;
    string profile_pic_link_1= "http://134.209.103.120/Casino/public";
    public string profile_pic_link_2;
    // Start is called before the first frame update
    void Start()
    {
        Get_image();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Get_image()
    {
        StartCoroutine("downloadImg", profile_pic_link_1+profile_pic_link_2);
    }

    IEnumerator downloadImg(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        profile_pic.sprite = image;
    }
}
