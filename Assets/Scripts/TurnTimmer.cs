﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TurnTimmer : MonoBehaviour
{
    public float TimeRemaining = 0;
    public float TurnTime = 0;
    public float income;
    public int TimeModifier;
    public UnityEngine.UI.Text TURS_LEFT_TimeText;
    public GameObject TURS_LEFT_Timetext;
    //public GameObject AudioTriggerSound;
    public int framesBeforeNextShot = 2;
    public int currentShotFrame = 0;

    void CountDown()
    {
        if (TimeRemaining <= 0)
        {
            TimeRemaining = TurnTime;
            // Add reward here
            ResetTimmer();
        }
        ShowTime();
        if (TimeRemaining > 0)
        {
            TimeRemaining = TimeRemaining - Time.deltaTime;
        }
    }
    void ResetTimmer()
    {
        //GameObject.FindGameObjectWithTag("MoneyS").GetComponent(MoneySystem).curMoney = GameObject.FindGameObjectWithTag("MoneyS").GetComponent(MoneySystem).curMoney += income;      
        if (currentShotFrame == 0)
        {
            //GameObject audioTriggerSound;
            //audioTriggerSound = (GameObject)Instantiate(AudioTriggerSound, this.transform.position, this.transform.rotation);
            //audioTriggerSound.transform.parent = this.transform;
        }
        else
        {
            currentShotFrame--;
        }
    }
    void ShowTime()
    {

        float mintues = TimeRemaining / 60;
        float seconds = TimeRemaining % 60;
        string timeString = mintues.ToString("00") + ":" + seconds.ToString("00");
        TURS_LEFT_TimeText.text = timeString;
    }
    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        CountDown();
    }
}
