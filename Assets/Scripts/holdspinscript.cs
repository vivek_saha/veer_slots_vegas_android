﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class holdspinscript : MonoBehaviour
{
    public static holdspinscript Instance;
    public int hold_delay;
    public static bool mouseDown = false;
    public float timeMouseDown;
    Slot slot;

    public Sprite stopSprite, normalsprite;
    public GameObject stop_button;
    private Button button;

    public int win_line_counter = 0;

    public bool autostart = false;


    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        GameObject a = GameObject.FindGameObjectWithTag("Slot");
        slot = a.GetComponent<Slot>();
        stop_button.SetActive(false);
        button = GetComponent<Button>();
    }
    public void Start()
    {
        GameObject a = GameObject.FindGameObjectWithTag("Slot");
        slot = a.GetComponent<Slot>();
        stop_button.SetActive(false);
        button = GetComponent<Button>();
    }



    void Update()
    {
        if (mouseDown)
        {
            timeMouseDown += Time.deltaTime;
        }
        if (timeMouseDown > hold_delay)
        {
            mouseDown = false;
            autostart = true;
            timeMouseDown = 0;
            button.interactable = false;
            Lvl_manager.Instance.disable_winline_but();
        }


    }

    private void FixedUpdate()
    {
        if (autostart)
        {
            if (Lvl_manager.Instance.Not_enough_Money_Panel.activeSelf)
            {
                //OnpointerClick();
                Stop_no_money();
            }
            else
            {
                switch (slot.state)
                {
                    case SlotState.playingwins:

                        button.interactable = true;
                        break;
                    case SlotState.ready:
                        button.interactable = true;
                        if (!Lvl_manager.Instance.Fiveofkindison)
                        {
                            try
                            {
                                slot.spin();
                            }
                            catch (Exception e)
                            {
                                GameObject a = GameObject.FindGameObjectWithTag("Slot");
                                slot = a.GetComponent<Slot>();
                                slot.spin();
                            }
                        }
                        break;
                    case SlotState.snapping:
                        button.interactable = true;
                        break;
                    case SlotState.spinning:
                        GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 0f);
                        stop_button.SetActive(true);
                        break;

                }
            }
        }
    }

    public IEnumerator WAitforwin()
    {
        yield return new WaitForSeconds(3f);
        slot.setState(SlotState.ready);

    }


    public void OnPointerDown()
    {
        mouseDown = true;
    }
    public void OnPointerUp()
    {
        mouseDown = false;
    }

    public void OnpointerClick()
    {
        if (slot.state == SlotState.ready || slot.state == SlotState.playingwins)
        {
            Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
            Audio_manager.Instance.slot_sound.Play();
        }
        if (stop_button.activeSelf)
        {
            mouseDown = false;
            autostart = false;
            timeMouseDown = 0;
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 1f);
            stop_button.SetActive(false);
            button.interactable = true;
            // GetComponent<Image>().sprite = normalsprite;
            if (slot.state != SlotState.ready && slot.state != SlotState.playingwins)
            {
                slot.spin();
            }
            Lvl_manager.Instance.enable_winline_but();
        }
        else
        {
            try
            {
                slot.spin();
                Lvl_manager.Instance.updateUI();
            }
            catch (Exception e)
            {
                GameObject a = GameObject.FindGameObjectWithTag("Slot");
                slot = a.GetComponent<Slot>();
                slot.spin();
                Lvl_manager.Instance.updateUI();
                print(e);
            }

        }
    }


    public void Stop_no_money()
    {

        mouseDown = false;
        autostart = false;
        timeMouseDown = 0;
        GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 1f);
        stop_button.SetActive(false);
        button.interactable = true;
        Lvl_manager.Instance.enable_winline_but();
    }



}
