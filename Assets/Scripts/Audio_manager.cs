﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Audio_manager : MonoBehaviour
{
    public static Audio_manager Instance;

    public AudioSource bg, button,slot_sound,reel_sound, slot_win;

    public AudioClip spin, reel_stop , lvl_up, lvl_click , fiveOK , slot_win_clip, daily_bonus , simple_but_click;


    private void Awake()
    {
        Instance = this;
    }



    // Start is called before the first frame update
    void Start()
    {
       if(PlayerPrefs.GetInt("music_toggle", 1)==1)
        {
            bg.enabled = true;
            bg.Play();
        }
       else
        {
            bg.enabled = false;
        }

        if (PlayerPrefs.GetInt("sound_toggle", 1) == 1)
        {
            button.enabled = true;

        }
        else
        {
            button.enabled = false;
        }

        if (PlayerPrefs.GetInt("slot_sound_toggle", 1) == 1)
        {
            slot_sound.enabled = true;
            reel_sound.enabled = true;
            slot_win.enabled = true;
        }
        else
        {
            slot_sound.enabled = false;
            reel_sound.enabled = false;
            slot_win.enabled = false;
        }



    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Simple_button_click()
    {
        button.clip = simple_but_click;
        button.Play();
       
    }
    
}
