﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Effects : MonoBehaviour
{

    public GameObject fireworks_1, fireworks_2, bubbles;



    GameObject a, b;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show_particle()
    {

        if (Lvl_manager.Instance.lvl_theme == 1)
        {
            bubbles.SetActive(true);
        }
        if (Lvl_manager.Instance.lvl_theme == 5)
        {
            //fireworks_1
            a = Instantiate(fireworks_1, new Vector3(-10.22f, 0.89f, 0.5f), Quaternion.identity);
            b = Instantiate(fireworks_2, new Vector3(4f, -5.5f, 0.5f), Quaternion.identity);
        }
    }


    public void hide_particle()
    {
        if (Lvl_manager.Instance.lvl_theme == 1)
        {
            bubbles.SetActive(false);
        }
        if (Lvl_manager.Instance.lvl_theme == 5)
        {
            Destroy(a);
            Destroy(b);
        }
    }

}
