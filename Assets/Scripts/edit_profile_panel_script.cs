﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class edit_profile_panel_script : MonoBehaviour
{

    public TMP_InputField input;
    public TextMeshProUGUI validation_msg;
    public Image pro_pic;


    // Start is called before the first frame update
    void Start()
    {
        pro_pic.sprite = Set_User_data.Instance.Profile_pic.sprite;
        input.text = Set_User_data.Instance.Name; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void on_Open()
    {
        pro_pic.sprite = Set_User_data.Instance.Profile_pic.sprite;
        input.text = Set_User_data.Instance.Name;
    }
    public void Onsave_click()
    {
        if(Input_Check())
        {
            //Get_Pic_data();
            Update_API_data.Instance.is_profile = 1;
            Update_API_data.Instance.username = input.text+Set_User_data.Instance.code;
            Update_API_data.Instance.Update_data();
            gameObject.SetActive(false);
        }
    }

    public bool Input_Check()
    {
        if(input.text==string.Empty)
        {
            //Debug.Log("empty");
            validation_msg.text = "Username Can't be Empty.";
            StartCoroutine("hide_validation_msg");
            return false;
        }
        else if(input.text.Length<4)
        {
            //Debug.Log("size");
            validation_msg.text = "Username Should Contain Atleast 4 Characters";
            StartCoroutine("hide_validation_msg");
            return false;
        }
        else
        {
            return true;
        }
    }

    IEnumerator hide_validation_msg()
    {
        yield return new WaitForSeconds(2f);
        validation_msg.text = string.Empty;
    }

 
}
