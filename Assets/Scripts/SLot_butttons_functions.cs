﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SLot_butttons_functions : MonoBehaviour
{
    private Slot slot;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void start_slot_buttons_functions()
    {
        GameObject a = GameObject.FindGameObjectWithTag("Slot");
        slot = a.GetComponent<Slot>();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void Spin()
    {
        slot.spin();
    }

    public void dec_line()
    {
        slot.refs.credits.decLinesPlayed();
    }

    public void inc_line()
    {
        slot.refs.credits.incLinesPlayed();
    }

    public void dec_bet()
    {
        slot.refs.credits.decBetPerLine();
    }


    public void inc_bet()
    {
        slot.refs.credits.incBetPerLine();
    }
    public void max_bet()
    {
        slot.refs.credits.betMaxPerLine();
    }
}
