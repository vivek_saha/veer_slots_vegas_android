﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class DailyReward : MonoBehaviour
{
    //UI
    public Text timeLabel;
    //only use if your timer uses a label
    public Button timerButton;
    //used to disable button when needed
    //public Image _progress;
    //public Sprite _closeTresr;
    //public Sprite _openTresr;

    //TIME ELEMENTS
    //public int hours;
    ////to set the hours
    //public int minutes;
    ////to set the minutes
    //public int seconds;
    //to set the seconds
    //private bool _timerComplete = false;
    //private bool _timerIsReady;
    //private TimeSpan _startTime;
    //private TimeSpan _endTime;
    //private TimeSpan _remainingTime;
    //progress filler
    //private float _value = 1f;
    //reward to claim
    //public int RewardToEarn;

    public GameObject disabled_button, popup_text, daily_bonus_panel, collect_but;

    [HideInInspector]
    public Image[] Chest;

    [HideInInspector]
    public Sprite Open_chest, closed_chest;

    [HideInInspector]
    public TextMeshProUGUI[] reward_text;

    public ParticleSystem bonus_bg_fireworks;

    public GameObject coin_1;

    public long add_coins;

    int current_chest;

    GameObject ps;

    //int day_diff;
    //int Counter
    //int Counter { get => PlayerPrefs.GetInt("Day_counter", 1); set => PlayerPrefs.SetInt("Day_counter", value); }

    //startup
    //void Start()
    //{
    //    if (PlayerPrefs.GetString("_timer") == "")
    //    {
    //        //Debug.Log("==> Enableing button");
    //        enableButton();
    //    }
    //    else
    //    {
    //        disableButton();
    //        StartCoroutine("CheckTime");
    //    }
    //}



    ////update the time information with what we got some the internet
    //private void updateTime()
    //{
    //    if (PlayerPrefs.GetString("_timer") == "Standby")
    //    {
    //        PlayerPrefs.SetString("_timer", TimeManager.sharedInstance.getCurrentTimeNow());
    //        PlayerPrefs.SetInt("_date", TimeManager.sharedInstance.getCurrentDateNow());
    //    }
    //    else if (PlayerPrefs.GetString("_timer") != "" && PlayerPrefs.GetString("_timer") != "Standby")
    //    {
    //        int _old = PlayerPrefs.GetInt("_date");
    //        int _now = TimeManager.sharedInstance.getCurrentDateNow();

    //        //check if a day as passed
    //        if (_now > _old)
    //        {//day as passed
    //         //day_diff=
    //         //Debug.Log("Day has passed");
    //            enableButton();
    //            return;
    //        }
    //        else if (_now == _old)
    //        {//same day
    //            //Debug.Log("Same Day - configuring now");
    //            _configTimerSettings();
    //            return;
    //        }
    //        else
    //        {
    //            Debug.Log("error with date");
    //            return;
    //        }
    //    }
    //    //Debug.Log("Day had passed - configuring now");
    //    _configTimerSettings();
    //}

    ////setting up and configureing the values
    ////update the time information with what we got some the internet
    //private void _configTimerSettings()
    //{
    //    _startTime = TimeSpan.Parse(PlayerPrefs.GetString("_timer"));
    //    _endTime = TimeSpan.Parse(hours + ":" + minutes + ":" + seconds);
    //    TimeSpan temp = TimeSpan.Parse(TimeManager.sharedInstance.getCurrentTimeNow());
    //    TimeSpan diff = temp.Subtract(_startTime);
    //    _remainingTime = _endTime.Subtract(diff);
    //    //start timmer where we left off
    //    setProgressWhereWeLeftOff();

    //    if (diff >= _endTime)
    //    {
    //        _timerComplete = true;
    //        enableButton();
    //    }
    //    else
    //    {
    //        _timerComplete = false;
    //        disableButton();
    //        _timerIsReady = true;
    //    }
    //}

    ////initializing the value of the timer
    //private void setProgressWhereWeLeftOff()
    //{
    //    float ah = 1f / (float)_endTime.TotalSeconds;
    //    float bh = 1f / (float)_remainingTime.TotalSeconds;
    //    _value = ah / bh;
    //    //_progress.fillAmount = _value;
    //}



    //enable button function
    private void enableButton()
    {
        //timerButton.GetComponent<Image>().sprite = _openTresr;

        disabled_button.SetActive(false);
        popup_text.SetActive(false);
        timerButton.interactable = true;
        timeLabel.text = "CLAIM REWARD";
    }


    //disable button function
    private void disableButton()
    {
        //timerButton.GetComponent<Image>().sprite = _closeTresr;
        disabled_button.SetActive(true);
        //popup_text.SetActive(true);

        timerButton.interactable = false;
        timeLabel.text = "NOT READY";
    }




    //use to check the current time before completely any task. use this to validate
    //private IEnumerator CheckTime()
    //{
    //    disableButton();
    //    timeLabel.text = "Checking the time";
    //    //Debug.Log("==> Checking for new time");
    //    yield return StartCoroutine(
    //        TimeManager.sharedInstance.getTime()
    //    );
    //    updateTime();
    //    //Debug.Log("==> Time check complete!");

    //}


    //trggered on button click
    public void rewardClicked()
    {

        daily_bonus_panel.SetActive(true);
        //Debug.Log("==> Claim Button Clicked");

    }

    //public void Turn_on_dailybonus_panel()
    //{

    //}



    //update method to make the progress tick
    //void Update()
    //{
    //    if (_timerIsReady)
    //    {
    //        if (!_timerComplete && PlayerPrefs.GetString("_timer") != "")
    //        {
    //            _value -= Time.deltaTime * 1f / (float)_endTime.TotalSeconds;
    //            //_progress.fillAmount = _value;

    //            //this is called once only
    //            if (_value <= 0 && !_timerComplete)
    //            {
    //                //when the timer hits 0, let do a quick validation to make sure no speed hacks.
    //                validateTime();
    //                _timerComplete = true;
    //            }
    //        }
    //    }
    //}



    ////validator
    //private void validateTime()
    //{
    //    //Debug.Log("==> Validating time to make sure no speed hack!");
    //    StartCoroutine("CheckTime");
    //}


    //private void claimReward(int x)
    //{
    //    //Debug.Log("YOU EARN " + x + " REWARDS");
    //}

    public void AlreadyClaimedBonus()
    {
        //open already claimed popup
        //print("open already claimed popup");
        popup_text.SetActive(true);
        StartCoroutine("closepopup");
    }

    public void Stop_couroutine_popUP()
    {
        StopCoroutine("closepopup");
    }
    IEnumerator closepopup()
    {
        yield return new WaitForSeconds(3.5f);
        popup_text.SetActive(false);
    }

    public void Onclicked_chest(int a)
    {
        Gamemanager.Instance.ads_controller.transform.GetComponent<AdsController>().Daily_reward = true;
        current_chest = a;
        //Chest[a].gameObject.transform.DOShakePosition(1f, 10f, 100).OnComplete(() => Open_chest_fun(a));
    }

    public void open_chest_after_ads()
    {
        Disable_all_chest();
        Chest[current_chest].gameObject.transform.DOShakePosition(1f, 10f, 100).OnComplete(() => Open_chest_fun(current_chest));
    }

    public void Open_chest_fun(int a)
    {
        //int n = UnityEngine.Random.Range(1, 10);
        //add_coins = Counter * 10000 * n;
        add_coins = UnityEngine.Random.Range(Get_API_Data_IAP.Instance.start,Get_API_Data_IAP.Instance.end);
        Audio_manager.Instance.button.clip = Audio_manager.Instance.daily_bonus;
        Audio_manager.Instance.button.Play();
        Chest[a].sprite = Open_chest;
        //dissable chests
        //Disable_all_chest();
        if (a == 0)
        {
            ps = Instantiate(coin_1, new Vector3((-1) * 6.68f, 0, 0), Quaternion.identity);
        }
        else if (a == 1)
        {
            ps = Instantiate(coin_1, Vector3.zero, Quaternion.identity);
        }
        else if (a == 2)
        {
            ps = Instantiate(coin_1, new Vector3(6.68f, 0, 0), Quaternion.identity);
        }
        //coin_1.Play();
        ps.GetComponent<ParticleSystem>().Play();
        reward_text[a].text = AbbrevationUtility.AbbreviateNumber(add_coins);
        reward_text[a].gameObject.transform.parent.gameObject.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        reward_text[a].gameObject.transform.parent.gameObject.SetActive(true);
        bonus_bg_fireworks.gameObject.SetActive(true);
        bonus_bg_fireworks.Play();
        PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits")) + add_coins).ToString());
        //Gamemanager.Instance.Setting_vlaues();
        //Counter++;
        StartCoroutine("Close_Bonus_panel",a);
    }
    IEnumerator Close_Bonus_panel(int a)
    {
        yield return new WaitForSeconds(3f);
        Gamemanager.Instance.save_scattersvalue();
        bonus_bg_fireworks.gameObject.SetActive(false);
        bonus_bg_fireworks.Stop();
        disableButton();
        GetComponent<Coin_effect_home_panel>().on_bonus_add();
        GetComponent<Coin_effect_home_panel>().credits_value = add_coins;
       //GetComponent<Floating_coin_script>().Move_up("+" +add_coins);
       daily_bonus_panel.SetActive(false);
        Chest[a].sprite = closed_chest;
        //enable_chest
        Enable_all_chest();
        reward_text[a].gameObject.transform.parent.gameObject.SetActive(false);
        reward_text[a].gameObject.transform.parent.gameObject.transform.DOScale(new Vector3(0, 0, 0), 0f);
        Destroy(ps);
        //claimReward(RewardToEarn);
        if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        {
            Update_API_data.Instance.is_update = 1;
            Update_API_data.Instance.type = "bonus";
            Update_API_data.Instance.Update_data();
        }
        //PlayerPrefs.SetString("_timer", "Standby");
        //StartCoroutine("CheckTime");
    }

    public void disable_dialy_bonus_fromgetapi_data()
    {
        disableButton();
    }
    public void enable_dialy_bonus_fromgetapi_data()
    {
        enableButton();
    }
    IEnumerator waiting()
    {
        yield return new WaitForSeconds(3f);
        Turn_onCollect_but();
    }
    //public void CLose_bonus_panel_()
    //{
    //    Gamemanager.Instance.save_scattersvalue();
    //    bonus_bg_fireworks.gameObject.SetActive(false);
    //    bonus_bg_fireworks.Stop();
    //    daily_bonus_panel.SetActive(false);
    //    collect_but.SetActive(false);
    //    Audio_manager.Instance.button.clip = Audio_manager.Instance.lvl_click;
    //    //Audio_manager.Instance.button.Play();
    //    Chest[current_chest].sprite = closed_chest;
    //    //enable_chest
    //    Enable_all_chest();
    //    reward_text[current_chest].gameObject.transform.parent.gameObject.SetActive(false);
    //    reward_text[current_chest].gameObject.transform.parent.gameObject.transform.DOScale(new Vector3(0, 0, 0), 0f);
    //    Destroy(ps);
    //    claimReward(RewardToEarn);
    //    if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
    //    {
    //        Update_API_data.Instance.is_update = 1;
    //        Update_API_data.Instance.type = "bonus";
    //        Update_API_data.Instance.Update_data();
    //    }
    //    PlayerPrefs.SetString("_timer", "Standby");
    //    StartCoroutine("CheckTime");
    //}
    public void Disable_all_chest()
    {
        for (int i = 0; i < 3; i++)
        {
            Chest[i].transform.parent.GetComponent<Button>().interactable = false;
        }
    }

    public void Enable_all_chest()
    {
        for (int i = 0; i < 3; i++)
        {
            Chest[i].transform.parent.GetComponent<Button>().interactable = true;
        }
    }

    public void Turn_onCollect_but()
    {
        //collect_but.SetActive(true);
    }
}