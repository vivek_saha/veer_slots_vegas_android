﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toggle_Sound_music : MonoBehaviour
{

    public static Toggle_Sound_music Instance;
    public bool sound_toggle
    {
        get
        {
            if (PlayerPrefs.GetInt("sound_toggle", 1) == 1)
            {
                return false;
            }
            else
            {
                return true;

            }

            //return PlayerPrefs.GetInt(); 
        }
        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("sound_toggle", 0);
            }
            else
            {
                PlayerPrefs.SetInt("sound_toggle", 1);
            }
        }
    }

    public bool music_toggle
    {
        get
        {
            if (PlayerPrefs.GetInt("music_toggle", 1) == 1)
            {
                return false;
            }
            else
            {
                return true;

            }

            //return PlayerPrefs.GetInt(); 
        }
        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("music_toggle", 0);
            }
            else
            {
                PlayerPrefs.SetInt("music_toggle", 1);
            }
        }
    }

    public bool slot_sound_toggle
    {
        get
        {
            if (PlayerPrefs.GetInt("slot_sound_toggle", 1) == 1)
            {
                return false;
            }
            else
            {
                return true;

            }

            //return PlayerPrefs.GetInt(); 
        }
        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("slot_sound_toggle", 0);
            }
            else
            {
                PlayerPrefs.SetInt("slot_sound_toggle", 1);
            }
        }
    }




    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name == "Sound_but")
        {
            GetComponent<Toggle>().isOn = !(Audio_manager.Instance.button.enabled);
            //toggle_sounds();
        }
        else if(gameObject.name == "Slot_Music_but")
        {
            GetComponent<Toggle>().isOn = !(Audio_manager.Instance.slot_sound.enabled);
        }
        else
        {
            GetComponent<Toggle>().isOn = !(Audio_manager.Instance.bg.enabled);
            //toggle_music();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void toggle_sounds()
    {
        Audio_manager.Instance.button.enabled = !(GetComponent<Toggle>().isOn);

        sound_toggle =! (Audio_manager.Instance.button.enabled);
    }
    public void toggle_music()
    {
        Audio_manager.Instance.bg.enabled = !(GetComponent<Toggle>().isOn);
        if (Audio_manager.Instance.bg.enabled)
        {
            if (!Audio_manager.Instance.bg.isPlaying) { Audio_manager.Instance.bg.Play(); }
        }
        music_toggle =!( Audio_manager.Instance.bg.enabled);
    }
    public void toggle_slot_Sound()
    {
        Audio_manager.Instance.slot_sound.enabled = !(GetComponent<Toggle>().isOn);
        Audio_manager.Instance.reel_sound.enabled= !(GetComponent<Toggle>().isOn);
        Audio_manager.Instance.slot_win.enabled = !(GetComponent<Toggle>().isOn);
        sound_toggle = !(Audio_manager.Instance.slot_sound.enabled);
    }
}
