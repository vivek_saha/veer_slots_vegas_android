﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Spin_coin_script : MonoBehaviour
{
    public RectTransform spin_start, spin_end,win_box_rect,buy_coin_start,lvl_up_collect_start;
    public GameObject coin_to_spin_prefab,coin_object,coin_effect_obj,winbox_obj;
    public Transform content;

    public long slot_win_value;

    // Start is called before the first frame update
    void Start()
    {
        //on_spin();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void on_spin()
    {
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        //Debug.Log(spin_start.position);
        //Vector3 loc = Camera.main.ScreenToWorldPoint(spin_start.position);
        temp.transform.position = spin_start.position;
        //Debug.Log(temp.transform.position);
        //temp.transform.SetParent(spin_content);
        //Debug.Log(spin_end.position);

        //RectTransformUtility.ScreenPointToWorldPointInRectangle(spin_end,)
        temp.GetComponent<RectTransform>().DOMove(spin_end.position, 01f).OnComplete(() => { Destroy(temp); });
    }

    public void onwin()
    {
        //win_effect();
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        //Debug.Log(spin_start.position);
        //Vector3 loc = Camera.main.ScreenToWorldPoint(spin_start.position);
        temp.transform.position = win_box_rect.position;
        //Debug.Log(temp.transform.position);
        //temp.transform.SetParent(spin_content);
        //Debug.Log(spin_end.position);

        //RectTransformUtility.ScreenPointToWorldPointInRectangle(spin_end,)
        temp.GetComponent<RectTransform>().DOMove(spin_start.position, 01f).OnComplete(() => { Destroy(temp);win_coin_size_changer(); });
    }

    public void coin_size_changer()
    {
        //GetComponent<Lvl_manager>().slot.refs.credits.awardWin(slot_win_value);
        coin_object.transform.localScale = Vector3.one;
        coin_object.transform.DOPunchScale(new Vector3(0.1f, 0.1f), 0.5f).OnComplete(() =>Coin_effect());
    }

    public void win_coin_size_changer()
    {
        GetComponent<Lvl_manager>().slot.refs.credits.awardWin(slot_win_value);
        GetComponent<Floating_coin_script>().Move_up("+"+slot_win_value);
        coin_object.transform.localScale = Vector3.one;
        coin_object.transform.DOPunchScale(new Vector3(0.1f, 0.1f), 0.5f).OnComplete(() => Coin_effect());
    }
    public void Coin_effect()
    {
        coin_effect_obj.SetActive(true);
        Invoke("stop_coin_effect", 1f);
    }

    public void stop_coin_effect()
    {
        coin_effect_obj.SetActive(false);
    }

    public void win_effect()
    {
        winbox_obj.GetComponent<Animator>().enabled = true;
        //winbox_obj.GetComponent<Animator>().Play("win_boxAnim");
        winbox_obj.SetActive(false);
        winbox_obj.SetActive(true);
        Invoke("stop_win_effect", 1f);
    }


    public void stop_win_effect()
    {
        winbox_obj.GetComponent<Animator>().enabled = false;
        
        onwin();
    }

    public void on_buy_coin()
    {
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = buy_coin_start.position;
        temp.GetComponent<RectTransform>().DOMove(spin_start.position, 01f).OnComplete(() => { Destroy(temp); coin_size_changer(); });
    }
    public void on_lvl_up_collect()
    {
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = lvl_up_collect_start.position;
        temp.GetComponent<RectTransform>().DOMove(spin_start.position, 01f).OnComplete(() => { Destroy(temp); coin_size_changer(); });
    }
}
